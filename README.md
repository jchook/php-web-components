# PHP Web Components

This is a very simple **view layer** for PHP applications.

This is just an outline to help you build your dream view renderer. Simply implement [`RendererInterface`](lib/Render/RendererInterface.php), or use [`AdvancedRenderer`](lib/Render/AdvancedRenderer.php) as a starting point.

## Try it

The example will work from command line:

```bash
php example.php
```

Or as a mini-HTTP server:

```bash
php -S 127.0.0.1:3000 example.php
```


## Concepts

Some key concepts might help.


### Everything is a component.

* Layouts
* Sections
* Groups of element
* Individual elements


### It's all "just PHP"

Or... phtml, pjs, pscss, anything...


### Renderers

None of the components render themselves. They only *describe* themselves.

All rendering is left to a single class called the Renderer. This class may delegate tasks, but is ultimately the final word and mediator in converting the abstract component tree (ACT) into a document such as HTML5.


### AdvancedRenderer

Currently the `AdvancedRenderer` will use a component folder tree to allow code organization per-component (e.g. phtml, js, scss, etc files, all together).

Example folder tree:

* `components`
	- `MainNav`
		- `MainNav.phtml`
		- `MainNav.scss`
		- `MainNav.js`
	- ...



## License

MIT.