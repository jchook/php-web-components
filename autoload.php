<?php

// Autoloader
spl_autoload_register(function($className){

	// Only load for this namespace
	if (strncmp($className, 'Web\\', 4) !== 0) {
		return;
	}

	// Simple directory structure based on namespace hierarchy
	if (file_exists($file = __DIR__ . '/lib/' . strtr(substr($className, 4), '\\', '/') . '.php')) {
		require $file;
	}

});
