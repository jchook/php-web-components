<?php

require __DIR__ . '/autoload.php';

use Web\Component\Component;
use Web\Render\AdvancedRenderer;

// Current request
$req = $_SERVER['REQUEST_URI'] ?? '/';

// Static assets?
if ($req !== '/') {
	$fpath = __DIR__ . $req;
	if (is_file($fpath)) {
		$file = fopen($fpath, 'r');
		if (substr($fpath, -4) === '.css') {
			header('Content-Type: text/css');
		}
		if (substr($fpath, -3) === '.js') {
			header('Content-Type: application/javascript');
		}
		fpassthru($file);
		return;
	}
} else {
	$req = '/home';
}

// Possibly the simplest router I've ever written...
$page = new Component(ucfirst(explode('/', $req)[1]));

// Advanced renderer
$r = new AdvancedRenderer(__DIR__ . '/components/');

// Render!
echo $r->render(
	new Component('Html5', ['id' => 'website'], [
		new Component('MainNav'),
		$page,
	])
);
