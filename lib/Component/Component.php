<?php

namespace Web\Component;

class Component implements ComponentInterface
{
	/**
	 * @var array
	 */
	protected $attributes;

	/**
	 * @var array
	 */
	protected $children;

	/**
	 * @var string
	 */
	protected $name = '';

	public function __construct(string $name, array $attributes = [], array $children = [])
	{
		$this->name = $name;
		$this->attributes = $attributes;
		$this->children = $children;
	}

	public function getName(): string
	{
		return $this->name;
	}

	public function getChildren(): array
	{
		return $this->children;
	}

	public function hasChildren(): bool
	{
		return (bool) $this->children;
	}

	public function getAttributes(): array
	{
		return $this->attributes;
	}
}