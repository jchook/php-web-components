<?php

namespace Web\Component;

interface ComponentInterface
{
	public function getName(): string;
	public function getChildren(): array;
	public function hasChildren(): bool;
	public function getAttributes(): array;
}