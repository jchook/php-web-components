<?php

namespace Web\Render;

/**
 * Renders a component tree as HTML
 */

use Web\Component\ComponentInterface;

class HtmlRenderer implements RendererInterface 
{
	private static $voidElements = [
		'area',
		'base',
		'br',
		'col',
		'command',
		'embed',
		'hr',
		'img',
		'input',
		'keygen',
		'link',
		'menuitem',
		'meta',
		'param',
		'source',
		'track',
		'wbr',
	];
	
	/**
	 * Void elements are "self-closing"
	 * @link https://www.w3.org/TR/html/syntax.html#void-elements
	 */
	protected static function isVoidElement($name): bool
	{
		return in_array($name, self::$voidElements);
	}
	
	/**
	 * Render a string, component, or array of both/either/neither
	 */
	public function render($mixed): string
	{
		if ($mixed instanceof ComponentInterface) {
			return $this->renderComponent($mixed);
		}
		if (is_string($mixed)) {
			return $mixed;
		}
		if (is_iterable($mixed)) {
			$html = [];
			foreach ($mixed as $element) {
				$html[] = $this->render($element);
			}
			return implode("\n", $html);
		}
		return '';
	}

	/**
	 * Render attributes
	 */
	public function renderAttributes(iterable $a): string
	{
		$attr = [];
		foreach ($a as $var => $val) {
			$attr[] = htmlspecialchars($var) . '=\'' . htmlspecialchars($val) . '\'';
		}
		return implode(' ', $attr);
	}
	
	/**
	 * Render a component
	 */
	protected function renderComponent(ComponentInterface $c): string
	{
		$name = $c->getName();
		$attr = $this->renderAttributes($c->getAttributes());
		$html = $name ? "<$name $attr>" : '';
		if (!$this::isVoidElement($name)) {
			if ($c->hasChildren()) {
				$html .= "\n" . $this->render($c->getChildren()) . "\n";
			}
			$html .= $name ? "</$name>" : '';
		}
		return $html;
	}
}