<?php

namespace Web\Render;

/**
 * 
 * Custom renderer that will automatically add css and js resources to an 
 * array and will also render components as a phtml layout instead of a generic
 * HTML tag.
 *
 * Modify and upgrade this file to your heart's content. It is here to serve.
 * 
 */

use Web\Component\ComponentInterface;

class AdvancedRenderer extends HtmlRenderer
{
	protected $componentsDir = '';
	protected $scripts = [];
	protected $styles = [];
	
	public function __construct(string $componentsDir = '')
	{
		// parent::__construct();
		$this->componentsDir = $componentsDir;
	}
	
	/**
	 * Render a component
	 *
	 * Do some fancy logic such as auto-adding scripts, etc
	 */
	protected function renderComponent(ComponentInterface $component): string
	{
		$name = $component->getName();
		$root = $this->componentsDir . $name . DIRECTORY_SEPARATOR;
		if (is_dir($root)) {
			if (file_exists($root . $name . '.css')) {
				$this->addStyle($root . $name . '.css');
			}
			if (file_exists($root . $name . '.js')) {
				$this->addScript($root . $name . '.js');
			}
			if (file_exists($root . $name . '.phtml')) {
				$renderer = $this;
				$content = $this->render($component->getChildren());
				ob_start();
				include $root . $name . '.phtml';
				return ob_get_clean();
			}
			return '';
		}
		return parent::renderComponent($component);
	}
	
	protected function addStyle(string $style): void
	{
		if (!in_array($style, $this->styles)) {
			$this->styles[] = $style;
		}
	}
	
	protected function addScript(string $script): void
	{
		if (!in_array($script, $this->scripts)) {
			$this->scripts[] = $script;
		}
	}
	
	public function getScripts(): array
	{
		return $this->scripts;
	}
	
	public function getStyles(): array
	{
		return $this->styles;
	}
}