<?php

namespace Web\Render;

/**
 * Render a component tree
 */

interface RendererInterface
{
	/**
	 * Render a string, component, or array of both/either/neither
	 */
	public function render($component): string;
}